/*global document */
var stage;
var circle;
var accel = 0;
var height;
var width;
var radius = 30;
var accel_d = 2;
var jump_d = 50;
var horiz = 0;
var horiz_d = 1;
var move_left = false;
var move_right = false;
var jump_next_frame = false;

function init() {
    "use strict";
    // Get height/width defined on the canvas object.
    var canvas = $('#canvas');
    width = canvas.prop('width');
    height = canvas.prop('height');

    stage = new createjs.Stage('canvas');

    draw(random_color(), radius * 2, radius * 2);

    createjs.Ticker.timingMode = createjs.Ticker.RAF_SYNCHED;
    createjs.Ticker.setFPS(30);
    createjs.Ticker.setInterval(5);
    createjs.Ticker.addEventListener('tick', tick);
    stage.update();
}

function random_color() {
    "use strict";
    var hue = Math.floor(Math.random() * 256);
    return 'hsl(' + hue + ', 100%, 50%)';
}

function change_ball_color() {
    "use strict";
    draw(random_color(), circle.x, circle.y);
}

function draw(color, x, y) {
    "use strict";
    if (circle !== undefined)
        stage.removeChild(circle);

    circle = new createjs.Shape();
    circle.graphics.f(color).dc(0, 0, radius);
    circle.x = x;
    circle.y = y;

    stage.addChild(circle);
}

function reduce_horiz(reduction) {
    if (horiz < 0)
        horiz += reduction;
    else if (horiz > 0)
        horiz -= reduction;
}

function tick(event) {
    "use strict";
    /* handle jumps */
    if (jump_next_frame) {
        accel -= jump_d;
        horiz += 0.1;
        jump_next_frame = false;
    }
    /* handle left/right */
    if (move_left)
        horiz -= 1;
    if (move_right)
        horiz += 1;

    /* Handle vertical (jumps) */
    if (circle.y < (height - radius) || accel < 0) {
        accel += accel_d;
        circle.y += accel;
    } else {
        /* Hit the ground */
        accel *= -1;
        if (accel !== 0)
            change_ball_color();
        /* Move position, if below ground (because of accel). */
        circle.y = height - radius;
        reduce_horiz(1);
    }
    if (circle.y < radius) {
        /* Hit the ceiling. */
        circle.y = radius;
        change_ball_color();
        accel = 0;
        reduce_horiz(Math.round((horiz < 0 ? horiz * -1 : horiz) / 2));
    }

    horiz = Math.round(horiz);
    circle.x += horiz;

    /* Check for left/right bounce */
    if (circle.x < radius || circle.x > width - radius) {
        horiz *= -1;
        draw(random_color(), circle.x, circle.y);
        if (circle.x < radius) {
            circle.x = radius;
            reduce_horiz(1);
        } else if (circle.x > width - radius) {
            circle.x = width - radius;
            reduce_horiz(1);
        }
    }

    if (accel === 0 && circle.y >= height - radius)
        reduce_horiz(3);

    stage.update(event);
}

/**********************/
/*  HANDLE KEY EVENTS */
/***********************/
$(function() {
    "use strict";
    $(window).keydown(function (e) {
        switch (e.which) {
        case 32: /* space */
            if (!jump_next_frame)
                jump_next_frame = true;
            e.preventDefault();
            break;
        case 37: /* left */
            if (!move_left)
                move_left = true;
            e.preventDefault();
            break;
        case 39:
            if (!move_right)
                move_right = true;
            e.preventDefault();
            break;
        }
    });
    $(window).keyup(function(e) {
        e.preventDefault();
        switch (e.which) {
        case 37: /* left */
            move_left = false;
            e.preventDefault();
            break;
        case 39: /* right */
            move_right = false;
            e.preventDefault();
            break;
        }
    });
});